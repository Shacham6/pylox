# PyLox
The project `PyLox` is my implementation 
of Lox, the language developed in Bob Nystrom's 
[Crafting Interpreters](https://craftinginterpreters.com) book.
